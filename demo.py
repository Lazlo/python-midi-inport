#!/usr/bin/python3

# TODO Allow clearing of mapping
# TODO Show all controls in list of mappings
# TODO Update screenshot and docmentation
# TODO Get rid of our MIDI message filter class
# TODO Simplify handlers (have only one handler per contol in best case)
# TODO Have controls have a glowing neon border on setup mode
# TODO In handlers for QPushButton, adjust to accept arg bool
#      so button states can be communicated to the handler.
# TODO Have MIDI icon inside MIDI button
# TODO Use "@" annotation to make click event handlers be midi event handlers
#      thereby automatically adding the setup code that is needed in the midi
#      handler callback.

import logging

from PyQt5.QtCore import QThread
from PyQt5.QtCore import pyqtSignal

class MidiIoWorker(QThread):

	message_received = pyqtSignal('PyQt_PyObject')

	def run(self):
		logging.info("Setting up MIDI port ...")
		import mido
		self.inport = mido.open_input()
		logging.info("Opened MIDI input port \"%s\"" % self.inport.name)
		for message in self.inport:
			self.message_received.emit(message)

from PyQt5.QtWidgets import QWidget

class View(QWidget):

	def __init__(self, midi):
		super().__init__()
		self.midi = midi
		self.initUI()

	def initUI(self):
		import sys
		import os
		title = os.path.basename(sys.argv[0])
		self.setWindowTitle(title)
		from PyQt5.QtWidgets import QHBoxLayout
		# Set the layout
		self.main_layout = QHBoxLayout()
		self.setLayout(self.main_layout)
		# Build sections
		self.initUI_controls()
		# Do MIDI related things
		from midimap import MidiSettingsWidget
		self.midi_settings_widget = MidiSettingsWidget(self.midi)
		self.main_layout.addWidget(self.midi_settings_widget)
		# Only after the mapping table was created
		# we can connect the table to the 'mapping_changed' signal
		# from the event mapper
		self.midi.mapping_changed.connect(self.midi_settings_widget._refresh_mapping_table)

	def initUI_controls(self):
		from PyQt5.QtWidgets import QGroupBox, QVBoxLayout
		from PyQt5.QtWidgets import QPushButton, QDial

		cg = QGroupBox("Controls")
		self.main_layout.addWidget(cg)

		cl = QVBoxLayout()
		cg.setLayout(cl)

		self.btn_f1 = QPushButton('F1', objectName='btn_f1')
		self.btn_f2 = QPushButton('F2', objectName='btn_f2')
		self.btn_f3 = QPushButton('F3', objectName='btn_f3')
		self.dial_f4 = QDial(objectName='dial_f4')
		self.dial_f5 = QDial(objectName='dial_f5')

		cl.addWidget(self.btn_f1)
		cl.addWidget(self.btn_f2)
		cl.addWidget(self.btn_f3)
		cl.addWidget(self.dial_f4)
		cl.addWidget(self.dial_f5)

		self.btn_f1.clicked.connect(self.handle_btn_f1_clicked)
		self.btn_f2.clicked.connect(self.handle_btn_f2_clicked)
		self.btn_f3.clicked.connect(self.handle_btn_f3_clicked)
		self.dial_f4.valueChanged.connect(self.handle_dial_f4_changed)
		self.dial_f5.valueChanged.connect(self.handle_dial_f5_changed)

	# Regular handlers

	def handle_btn_f1_clicked(self):
		if self.midi.setup_controls_mapping('btn_f1'):
			return
		logging.info("View: F1 clicked")

	def handle_btn_f2_clicked(self):
		if self.midi.setup_controls_mapping('btn_f2'):
			return
		logging.info("View: F2 clicked")

	def handle_btn_f3_clicked(self):
		if self.midi.setup_controls_mapping('btn_f3'):
			return
		logging.info("View: F3 clicked")

	def handle_dial_f4_changed(self, value):
		if self.midi.setup_controls_mapping('dial_f4'):
			return
		logging.info("View: F4 changed (%s)" % value)

	def handle_dial_f5_changed(self, value):
		if self.midi.setup_controls_mapping('dial_f5'):
			return
		logging.info("View: F5 changed (%s)" % value)

	# MIDI handlers

	def handle_btn_f1_midi(self, msg):
		self.midi._handle_btn_common_midi('btn_f1', msg)

	def handle_btn_f2_midi(self, msg):
		self.midi._handle_btn_common_midi('btn_f2', msg)

	def handle_btn_f3_midi(self, msg):
		self.midi._handle_btn_common_midi('btn_f3', msg)

	def handle_dial_f4_midi(self, msg):
		self.midi._handle_dial_common_midi('dial_f4', msg)

	def handle_dial_f5_midi(self, msg):
		self.midi._handle_dial_common_midi('dial_f5', msg)

def main():
	import sys
	from optparse import OptionParser
	parser = OptionParser()
	parser.add_option("-g", "--debug", dest="debug", default=False,
				action="store_true",
				help="Enable debug output")
	(options, args) = parser.parse_args()
	if options.debug:
		loglevel=logging.DEBUG
	else:
		loglevel=logging.INFO
	logging.basicConfig(level=loglevel)
	import threading
	logging.debug("[%s]:%s()" % (threading.currentThread(), 'main'))
	from PyQt5.QtWidgets import QApplication
	app = QApplication([])


	from midimap import MidiEventMapper
	logging.info("Setting up MIDI Event Mapper ...")
	midi = MidiEventMapper()
	# Try to load mappings automatically - if there are none just keep going.
	midi.load()

	v = View(midi)

	# Setup available controls to be made available to the MIDI mapper
	midi.add_control('btn_f1', v.btn_f1, v.handle_btn_f1_midi)
	midi.add_control('btn_f2', v.btn_f2, v.handle_btn_f2_midi)
	midi.add_control('btn_f3', v.btn_f3, v.handle_btn_f3_midi)
	midi.add_control('dial_f4', v.dial_f4, v.handle_dial_f4_midi)
	midi.add_control('dial_f5', v.dial_f5, v.handle_dial_f5_midi)

	midi_iothread = MidiIoWorker()
	midi_iothread.message_received.connect(v.midi.handle_msg)
	midi_iothread.start()


	v.show()
	app.exec_()

if __name__ == '__main__':
	main()
