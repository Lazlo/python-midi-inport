# MIDI Input Port using Python

Purpose of this application is to give an example how to create
a MIDI input port that allows receiving MIDI messages from devices
to allow for controlling what the application does via MIDI.

It uses Python 3 and the [Mido library](https://github.com/mido/mido) (requires python-rtmidi and Jack2).

To set up all dependencies run the setup.sh script as root.

```
sudo ./setup.sh
```

To see the demo application perform its work follow these steps:

 * start some software MIDI instrument (like VMPK) or connect some MIDI equipment to your computer
 * start the demo application (```./demo.py```)
 * launch the JACK control application QJackCtl (actually launching is enough - no need to press the "Start" button)
   - click on the "Connect" button
   - click on the "ALSA" tab
   - drag the entry from the list of "Readable Clients / Output Ports" that represents your MIDI device
   - drop it onto the the "Writable Client / Input Ports" entry called "RtMidiIn Client"
 * change any of the controls on your MIDI instrument
 * observe the output of the terminal window in which main.py is running

This is how the "Connections" in QJackCtl should look like:

![QJackCtlConnections](doc/images/qjackctl-connections.png)

This is the output produced by main.py when a controls of the MIDI device are operated:

![Terminal main.py output](doc/images/term-main-py-output.png)
