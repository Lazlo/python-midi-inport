#!/usr/bin/python3

import unittest
from unittest.mock import Mock

from midimap import MidiEventMapper
from midimap import ControlsEntry

class FakeColor():

	def color(self, c):
		return

class FakeWidget():

	def setAutoFillBackground(self, on):
		return

	def palette(self):
		return FakeColor()

class MidiEventMapperAbstractTestCase():

	def create_msg(self, msg_type):
		import mido
		return mido.Message(msg_type)

	def add_mapping(self, control, msg):
		self.m.toggle_setup()
		self.m.start_mapping(control)
		self.m.finalize_mapping(msg)
		self.m.toggle_setup()

class MidiEventMapperAddControl(unittest.TestCase, MidiEventMapperAbstractTestCase):

	def setUp(self):
		expected_objectName = 'bar'
		expected_widget = FakeWidget()
		self.mock = Mock()
		self.handler = self.mock.handler()
		self.m = MidiEventMapper()
		self.m.add_control(expected_objectName, expected_widget, self.handler)

	def testControlWasAdded(self):
		self.assertEqual(len(self.m._controls), 1)

class MidiEventMapperCreateMapping(unittest.TestCase, MidiEventMapperAbstractTestCase):

	def setUp(self):
		self.m = MidiEventMapper()

	def testMappingWasCreated(self):
		expected_objectName = 'foo'
		expected_msgType = 'note_on'
		expected_midiChannel = '0'
		expected = [[expected_objectName, '0', expected_msgType, expected_midiChannel]]
		# Create control object to allow mapping to
		control = ControlsEntry(expected_objectName, None, None, None)
		# Create MIDI message
		msg = self.create_msg(expected_msgType)
		# Do the mapping
		self.m.toggle_setup()
		self.m.start_mapping(control)
		self.m.finalize_mapping(msg)
		# Verify
		actual = self.m._mappings.to_arr()
		self.assertEqual(actual, expected)

	# Test when a new control/widget is mapped to a message
	# that was mapped to another control/widget before, the previous
	# mapping is removed.
	def testReMappingRemovesExistingMapping(self):
		widget1 = FakeWidget()
		control1 = ControlsEntry('first', widget1, None, None)
		widget2 = FakeWidget()
		control2 = ControlsEntry('second', widget2, None, None)
		self.add_mapping(control1, self.create_msg('note_on'))
		self.add_mapping(control2, self.create_msg('note_on'))
		self.assertEqual(self.m._mappings.rowCount(), 1)

class MidiEventMapperTriggersEvent(unittest.TestCase, MidiEventMapperAbstractTestCase):

	def setUp(self):
		# Create fake widget
		expected_objectName = 'power'
		fake_widget = FakeWidget()
		self.mock = Mock()
		self.handler = self.mock.handler()
		# Create fake control dict
		fake_control = ControlsEntry(expected_objectName, fake_widget, self.handler, None)
		# Create MIDI message
		expected_msgType = 'note_on'
		msg = self.create_msg(expected_msgType)
		# Set up the mapping
		self.m = MidiEventMapper()
		self.m.add_control(expected_objectName, fake_widget, self.handler)
		self.add_mapping(fake_control, msg)

	# TODO Make sure the handler is only called on the expected
	# MIDI message

	def testHandlerIsCalledOnMessage(self):
		# Create a message to trigger the handler
		msg = self.create_msg('note_on')
		self.m.handle_msg(msg)
		self.handler.assert_called()

	def testHandlerIsNotCalled(self):
		# Create a message which has no corresponding
		# mapping configured in the mapper.
		msg = self.create_msg('note_off')
		self.m.handle_msg(msg)
		self.handler.assert_not_called()

	def testHandlerIsCalledOfLastWidget(self):
		# Create 2nd fake widget
		expected_objectName2 = 'reset'
		fake_widget2 = FakeWidget()
		self.handler2 = self.mock.handler2()
		# Create 2nd fake control dict
		fake_control2 = ControlsEntry(expected_objectName2, fake_widget2, self.handler2, None)
		# Create MIDI message to map contol to
		expected_msgType = 'note_on'
		msg = self.create_msg(expected_msgType)
		self.m.add_control(expected_objectName2, fake_widget2, self.handler2)
		self.add_mapping(fake_control2, msg)
		# Execrcise
		self.m.handle_msg(msg)
		# verify 2nd control handler is called
		self.handler2.assert_called()

if __name__ == '__main__':
	unittest.main()
