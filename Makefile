TESTCMD = python3 -m unittest discover -s test
VTESTCMD = $(TESTCMD) -v

test:
	$(TESTCMD)
vtest:
	$(VTESTCMD)
clean:
	$(RM) -rf __pycache__ */__pycache__

.PHONY: test vtest clean
