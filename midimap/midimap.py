import logging

class MidiMessageFilter():

	@staticmethod
	def create(msg):
		"""Used generalize a message for comparison in filter matching."""
		# Create a dict from the message object
		if msg.type in ['note_on', 'note_off']:
			# Remove 'time' and 'velocity' so only
			# 'type', 'channel' and 'note' will remain.
			return msg.copy(time=0, velocity=0)
		elif msg.type in ['control_change']:
			# Remove 'time' and 'value' so only
			# 'type', 'channel' and 'control' will remain.
			return msg.copy(time=0, value=0)
		elif msg.type in ['pitchwheel']:
			# Remove 'time' so only
			# 'type' and 'channel' will remain.
			return msg.copy(time=0)

class MappingsEntry(object):

	def __init__(self, objectName, f):
		self.objectName = objectName
		self.filter = f

from PyQt5.QtCore import QAbstractTableModel, QModelIndex
from PyQt5.QtCore import Qt, QVariant

class MidiMappingModel(QAbstractTableModel):

	def __init__(self):
		super().__init__()
		self._mappings = {}
		self.my_columns = ['Input', 'Control/Note', 'Type', 'Channel']
		self.last_control = None

	# File IO Methods

	def save(self):
		"""Stores mappings in a JSON file."""
		m = self.to_arr()
		if len(m) == 0:
			return
		import json
		txt = json.dumps(m, indent=8)
		path = 'midi-mappings-settings.json'
		f = open(path, 'w')
		f.write(txt)
		f.close()
		logging.info("Wrote mappings to %s" % path)

	def load(self, path='midi-mappings-settings.json'):
		logging.info("Reading mappings from %s" % path)
		import os
		if not os.path.exists(path):
			return
		f = open(path)
		txt = f.read()
		import json
		for entry in json.loads(txt):
			logging.debug(entry)
			# Create mapping entry object
			objectName = entry[0]
			note_or_cid = int(entry[1])
			msgType = entry[2]
			channel = int(entry[3])
			import mido
			m = mido.Message(msgType)
			if msgType in ['note_on', 'note_off']:
				m.note = note_or_cid
			elif msgType in ['control_change']:
				m.control = note_or_cid
			else:
				logging.warning("No attrs defined when restoring mapping for message type %s!" % msgType)
			m.channel = channel
			f = MidiMessageFilter.create(m)
			me = MappingsEntry(objectName, f)
			mp = {objectName: me}
			# Update mappings
			self._mappings.update(mp)

	def get_mapping_by_msg(self, msg):
		f = MidiMessageFilter.create(msg)
		for k in self._mappings.keys():
			mp_filter = self._mappings[k].filter
			if mp_filter != f:
				continue
			return self._mappings[k]

	def delete_by_filter(self, f):
		delete_mappings = []
		for k in self._mappings.keys():
			if self._mappings[k].filter != f:
				continue
			# Mark mapping for deletion
			delete_mappings.append(k)
		# Actually delete the mappings
		for m in delete_mappings:
			self._mappings.pop(m)

	def start_mapping(self, control):
		"""Expects argument to be of type ControlsEntry"""
		if type(control) != ControlsEntry:
			logging.error("Expected control to be of type ControlsEntry but was %s" % type(control))
			return
		self.last_control = control

	def finalize_mapping(self, msg):
		if self.last_control == None:
			return
		# Find existing mappings using the same filter
		f = MidiMessageFilter.create(msg)
		self.delete_by_filter(f)
		# Create a new mapping
		c = self.last_control
		objectName = c.objectName
		mp = {objectName: MappingsEntry(objectName, f)}
		self._mappings.update(mp)

	def to_arr(self):
		arr = []
		for k in self._mappings.keys():
			# Get mapping
			mp = self._mappings[k]
			f = mp.filter
			# Construct row
			row = []
			row.append("%s" % mp.objectName)
			if f.type == 'control_change':
				row.append("%d" % f.control)
			elif f.type in ['note_on', 'note_off']:
				row.append("%d" % f.note)
			else:
				row.append("%s" % mp.filter)
			row.append("%s" % f.type)
			row.append("%d" % f.channel)
			arr.append(row)
		return arr

	#
	# QAbstractTableModel interface
	#

	def rowCount(self, parent=QModelIndex):
		return len(self._mappings)

	def columnCount(self, parent=QModelIndex):
		return len(self.my_columns)

	def data(self, index, role):
		if role != Qt.DisplayRole:
			return
		row = index.row()
		col = index.column()
		val = self.to_arr()[row][col]
		return QVariant(val)

	def headerData(self, section, orientation, role=Qt.DisplayRole):
		if role == Qt.DisplayRole and orientation == Qt.Horizontal:
			return self.my_columns[section]

class ControlsEntry(object):

	def __init__(self, objectName, widget, midi_handler, default_bg):
		self.objectName = objectName
		self.widget = widget
		self.midi_handler = midi_handler
		self.default_bg = default_bg

from PyQt5.QtCore import QObject
from PyQt5.QtCore import pyqtSignal

class MidiEventMapper(QObject):
	"""Responsible for setting up and triggering callbacks
	on MIDI messages received."""

	mapping_changed = pyqtSignal()

	def __init__(self, ignore_note_off=True):
		super().__init__()
		# If we are in setup mode (connecting widgets to MIDI messages)
		self._setup = False
		self._ignore_note_off = ignore_note_off
		logging.info("NOTE note_off messages will be ignored")
		# The controls list uses the objectName of the widget as the key
		# The value contains a dict with a referenc to the widget and
		# its callback function.
		# e.g.
		# {
		#   'pwr_btn': {'objectName': 'pwr_btn', 'widget': <someRefToQtWidget>, 'midi_handler': somehandlerfnc},
		#   'rst_btn': {'objectName': 'rst_btn', 'widget': <someRefToQtWidget>, 'midi_handler': someotherhandlerfnc}
		# }
		self._controls = {}
		# A list of mappings between MIDI messages and widgets
		# from the _controls list. This list will not keep a reference to the
		# widget but use the objectName an identifier to locate the widget
		# reference within the _controls list.
		self._mappings = MidiMappingModel()
		# colors
		from PyQt5.QtCore import Qt
		self.color_mappable = Qt.magenta
		self.color_current = Qt.yellow
		self.color_mapped = Qt.green

	def load(self):
		self._mappings.load()

	# Widget Style Methods

	def _get_widget_bgcolor(self, w):
		from PyQt5.QtGui import QPalette
		color = w.palette().color(QPalette.Background)
		return color

	def _set_widget_bgcolor(self, w, color):
		p = w.palette()
		p.setColor(w.backgroundRole(), color)
		w.setPalette(p)

	def _toggle_controls_bgcolor(self, midi_mapping_mode):
		for k in self._controls.keys():
			c = self._controls[k]
			w = c.widget
			if midi_mapping_mode:
				if c.objectName in self._mappings._mappings.keys():
					bg_color = self.color_mapped
				else:
					bg_color = self.color_mappable
			else:
				bg_color = c.default_bg
			self._set_widget_bgcolor(w, bg_color)

	def add_control(self, objectName, widget, handler):
		"""Call to make a widget available for mapping.

		This method stores a reference to the widget instance.
		"""

		# Set common attributes
		widget.setAutoFillBackground(True)

		# Save original background color
		bg = self._get_widget_bgcolor(widget)
		# Create controls entry
		c = ControlsEntry(objectName, widget, handler, bg)
		# Save in list of controls
		self._controls.update({objectName: c})

	def _get_control_by_objectName(self, objectName):
		if not objectName in self._controls.keys():
			logging.warning("No control found with objectName %s" % objectName)
			return
		c = self._controls[objectName]
		return c

	def setup_controls_mapping(self, objectName):
		if not self._setup:
			return False
		c = self._get_control_by_objectName(objectName)
		if c == None:
			return False
		# Set (re-)set all mappable controls to have
		# special backgorund color
		self._toggle_controls_bgcolor(self._setup)
		# Set current control to have a even more special color
		self._set_widget_bgcolor(c.widget, self.color_current)
		# Finally start the mapping
		self.start_mapping(c)
		return True

	def toggle_setup(self):
		"""Call this method to enter setup mode.
		In this mode, the last MIDI messages will be
		saved as the trigger for the control element
		that changed last."""
		# If setup was enabled and is about to be disabled
		# we need to at least clear the last widget.
		# Otherwise next time setup is enabled mappings
		# will be created for the last widget on every MIDI message.
		if self._setup:
			self._mappings.last_control = None
		# Invert current setup state
		self._setup = not self._setup
		if self._setup:
			state = 'enabled'
		else:
			state = 'disabled'
		logging.info('MIDI Event Mapper: setup %s' % state)

	def start_mapping(self, control):
		"""Called by the UI to tell the mapper for which control element
		the next MIDI message should be used to create a mapping.

		Currently it is the responsibility of the user of this
		object to call this method within a event handler for
		a widget in the view class."""
		if not self._setup:
			logging.warning("start_mapping() called when setup was False!")
			return
		logging.debug("last_control %s" % control)
		self._mappings.start_mapping(control)

	def finalize_mapping(self, msg):
		"""Called when a MIDI message is received in setup mode.

		This method will create a mapping between the last control element
		that was active and the MIDI message passed to this method."""
		if not self._setup:
			logging.warning("finalize_mapping() called when setup was False!")
			return

		logging.debug("msg = %s" % msg)
		self._mappings.finalize_mapping(msg)

		# Turn widget green
		# By geting the mapping by message/filter
		# then using the mapping to obtain the controls
		# entry and finally setting the widget background color.
		mp = self._mappings.get_mapping_by_msg(msg)
		c = self._get_control_by_objectName(mp.objectName)
		self._set_widget_bgcolor(c.widget, Qt.green)

		logging.debug("Current mapping:")
		for row in self._mappings.to_arr():
			logging.debug("mapping %s" % row)
		# Update the mapping table in the view
		self.mapping_changed.emit()

	def _handle_btn_common_midi(self, objectName, msg):
		c = self._get_control_by_objectName(objectName)
		if c == None:
			return
		w = c.widget
		from PyQt5.QtWidgets import QPushButton
		if type(w) == QPushButton:
			w.animateClick()
		else:
			logging.warning("No action defined for %s" % type(w))

	def _handle_dial_common_midi(self, objectName, msg):
		"""Set dial widget value from MIDI message."""
		c = self._get_control_by_objectName(objectName)
		if c == None:
			return
		w = c.widget
		if msg.type == 'control_change':
			v = msg.value
		elif msg.type == 'pitchwheel':
			v = msg.pitch
		else:
			return
		w.setValue(v)

	def dispatch_msg(self, msg):
		"""Trigger a change in the UI if a MIDI mapping exists."""

		m = self._mappings.get_mapping_by_msg(msg)
		if not m:
			return
		k = m.objectName
		logging.debug("Dispatching %s to %s" % (msg, k))
		# Find the entry in the _controls list to get the reference
		# to the callback for this widget.
		c = self._get_control_by_objectName(k)
		if c == None:
			return
		cb = c.midi_handler
		cb(msg)

	def handle_msg(self, msg):
		"""Callback to mido.open_input(callback=)

		This method is not meant to be calle by the user. Instead a reference
		to this method is passed to the MIDI input port as the callback for
		when a MIDI message is received."""

		if not self._setup:
			self.dispatch_msg(msg)
		else:
			# Only in setup, we might want to ignore note_off
			# MIDI messages. Otherwise it will not be possible
			# to assign a mapping for the note_on message, as
			# when performing manual mapping, a note ON message
			# is always followed by an note OFF messages.
			if self._ignore_note_off and msg.type == 'note_off':
				return
			self.finalize_mapping(msg)

from PyQt5.QtWidgets import QGroupBox

class MidiSettingsWidget(QGroupBox):

	def __init__(self, midi):
		label = "Settings"
		super().__init__(label)
		self.midi = midi
		self.initUI()

	def initUI(self):
		from PyQt5.QtWidgets import QHBoxLayout, QVBoxLayout, QGroupBox
		from PyQt5.QtWidgets import QPushButton, QDial, QTableView

		#
		# Settings
		#

		ml = QVBoxLayout()
		self.setLayout(ml)

		# Add MIDI Mapping setup button
		btn_midi = QPushButton('MIDI')
		btn_midi.setCheckable(True)
		btn_midi.clicked.connect(self.handle_btn_midi_clicked)
		ml.addWidget(btn_midi)

		# Action buttons

		# Put buttons into group
		g2 = QGroupBox("MIDI Mapping")
		ml.addWidget(g2)
		l2 = QHBoxLayout()
		g2.setLayout(l2)

		self.btn_load_mappings = QPushButton("Load")
		self.btn_load_mappings.clicked.connect(self.handle_btn_load_clicked)
		l2.addWidget(self.btn_load_mappings)
		self.btn_save_mappings = QPushButton("Save")
		self.btn_save_mappings.clicked.connect(self.handle_btn_save_clicked)
		l2.addWidget(self.btn_save_mappings)

		# Populate model
		# MIDI Mapping Table
		self.mappings_table = t = QTableView()
		t.horizontalHeader().setStretchLastSection(True)
		# MIDI Mappings Table Model
		t.setModel(self.midi._mappings)
		ml.addWidget(t)

	def _refresh_mapping_table(self):
		self.midi._mappings.layoutAboutToBeChanged.emit()
		self.midi._mappings.layoutChanged.emit()

	def handle_btn_midi_clicked(self):
		self.midi.toggle_setup()
		# When MIDI mode is enabled, set the background
		# color of all controls that can be mapped to some
		# non-standard color.
		self.midi._toggle_controls_bgcolor(self.midi._setup)
		# Unselect rows from mapping table
		if not self.midi._setup:
			self.mappings_table.clearSelection()

	def handle_btn_load_clicked(self):
		logging.info("Load clicked")
		self.midi._mappings.load()
		self._refresh_mapping_table()

	def handle_btn_save_clicked(self):
		logging.info("Save clicked")
		self.midi._mappings.save()
