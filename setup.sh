#!/bin/bash

set -e
set -u

pkgs=""
pkgs="$pkgs python3"
pkgs="$pkgs python3-pip"
pkgs="$pkgs python3-pyqt5"
pkgs="$pkgs libjack-jackd2-dev"

apt-get -q install -y $pkgs
pip3 install --system -r requirements.txt
